package persons;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "PERSONS",
        indexes = {@Index(name = "BIRTHDAY_INDEX", columnList = "BIRTHDAY"),
                @Index(name = "LAST_NAME_INDEX", columnList = "LAST_NAME")}


)
@SecondaryTable(name = "URLS")


public class Person {


    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name = "ADDRESS_STREET")),
            @AttributeOverride(name = "number", column = @Column(name = "ADDRESS_NUMBER")),
            @AttributeOverride(name = "zipCode", column = @Column(name = "ADDRESS_ZIPCODE")),
            @AttributeOverride(name = "city", column = @Column(name = "ADDRESS_CITY")),
            @AttributeOverride(name = "country", column = @Column(name = "ADDRESS_COUNTRY"))})
private Address address = new Address();

    @Column(name = "ID")
    @Id
    @GeneratedValue
    private int id;
    @Version
    @Column(name = "VERSION", nullable = false)
    private int version;

    @Column(length = 40, name = "FIRST_NAME", nullable = false)
    private String firstName;

    @Column(length = 40, name = "LAST_NAME", nullable = false)
    private String lastName;

    @Column(name = "BIRTHDAY")
    private LocalDate birthDay;

    @Column(length = 10, name = "GENDER", nullable = false)
    @Enumerated(EnumType.STRING)
    private GenderType gender;


    @Column(name = "PICTURE")
    @Lob
    private byte[] picture;


    @Column(name = "COMMNT")
    @Lob
    private String comment;

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    @Column(name = "MARRIED")
    private Boolean married;

    private int age;

    @Column(table = "URLS", name = "HOMEPAGE", length = 255)
    private String homePage;


    public Person() {
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getMarried() {
        return married;
    }

    public void setMarried(Boolean married) {
        this.married = married;
    }

    @Transient
    public int getAge() {
        LocalDate now = LocalDate.now();
        return (int) ChronoUnit.YEARS.between(birthDay, now);
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getHomePage() {
        return homePage;
    }

    public void setHomePage(String homePage) {
        this.homePage = homePage;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
