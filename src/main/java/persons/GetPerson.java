package persons;

import messages.Message;

import javax.persistence.*;
import java.util.List;

public class GetPerson {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = null;
        EntityManager entityManager = null;

        try{
            entityManagerFactory = Persistence.createEntityManagerFactory("course");
            entityManager = entityManagerFactory.createEntityManager();
            EntityTransaction entityTransaction = entityManager.getTransaction();

            entityTransaction.begin();
            // GET QUERY IN CACHE
            TypedQuery<Person> query = entityManager.createQuery("select p from Person as p",Person.class);
            List<Person> persons = query.getResultList();

            System.out.println(entityManagerFactory.getCache().contains(Message.class,1L));
            for (Person p: persons){
                System.out.println(p.getFirstName());
            }

            /*
            Message message = entityManager.find(Message.class, 1L);
           // System.out.println(message.getText());

            //ROW AVAILABLE IN CACHE???
            System.out.println(entityManagerFactory.getCache().contains(Message.class,1L));
            entityManagerFactory.getCache().evict(Message.class,1L);
            System.out.println(entityManagerFactory.getCache().contains(Message.class,1L));*/
            entityTransaction.commit();


        }finally {
            if (entityManager != null){
                entityManager.close();
            }
            if (entityManagerFactory !=null){
                entityManagerFactory.close();
            }
        }

    }
}
