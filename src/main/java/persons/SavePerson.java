package persons;

import credits.Credit;

import javax.persistence.*;
import javax.swing.*;
import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;




public class SavePerson {


    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {

            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();

            tx.begin();
            Person person = new Person();
            Address address = new Address();

            String firstName = JOptionPane.showInputDialog("Enter first name","Omar");
            String lastName = JOptionPane.showInputDialog("Enter last name","Schultink");
            String birthDay = JOptionPane.showInputDialog("Enter birthday in dd/MM/yyyy","12/09/1981");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate localDateBirthDay = LocalDate.parse(birthDay, formatter);
            String gender = JOptionPane.showInputDialog("Enter gender (male/female)","male");
            String comment = JOptionPane.showInputDialog("Enter comments");
            String married = JOptionPane.showInputDialog("Are you married? (y/n)","n");
            String homePage = JOptionPane.showInputDialog("Enter your homepage URL","http://");

            String street = JOptionPane.showInputDialog("Enter streetname","Hoek");
            String number = JOptionPane.showInputDialog("Enter housenumber","2");
            String zipCode = JOptionPane.showInputDialog("Enter zipcode","3930");
            String city = JOptionPane.showInputDialog("Enter city","Achel");
            String country = JOptionPane.showInputDialog("Enter country","Belgium");



            person.setFirstName(firstName);
            person.setLastName(lastName);
            person.setBirthDay(localDateBirthDay);
            person.setAge(person.getAge());

            switch (gender) {
                case "male":
                    person.setGender(GenderType.MALE);
                    break;
                case "female":
                    person.setGender(GenderType.FEMALE);
                    break;
                default:
                    person.setGender(GenderType.UNKNOWN);
                    break;
            }

            person.setComment(comment);

            switch (married) {
                case "y":
                    person.setMarried(true);
                case "n":
                    person.setMarried(false);
            }

            person.setHomePage(homePage);
            address.setStreet(street);
            address.setNumber(number);
            address.setZipCode(zipCode);
            address.setCity(city);
            address.setCountry(country);
            person.setAddress(address);



            try {
                File image = new File("C:\\Users\\Java08\\IdeaProjects\\Servlet\\Hibernate-JPA\\src\\main\\resources\\images\\nl.jpg");
                FileInputStream inputStream = new FileInputStream(image);
                person.setPicture(inputStream.readAllBytes());
            } catch (IOException exception) {
                exception.printStackTrace();
            }


            em.persist(person);
            tx.commit();


        } finally {
            if (em != null) {
                em.close();
            }
            if (emf != null) {
                emf.close();
            }
        }

    }
}
