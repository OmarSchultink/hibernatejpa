package nationalSoccerTeams.teams;

import persons.Address;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;


@Entity
@Table(name = "NATIONAL_TEAMS_EC2020")
public class Player {

    //VARIABLES (COLUMNS)

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    @Column(name = "COUNTRY", length = 40, nullable = false)
    @Enumerated(EnumType.STRING)
    private Country country;

    @Column(name = "POSITION", length = 10)
    @Enumerated(EnumType.STRING)
    private Position position;

    @Column(name = "FIRST_NAME", length = 40, nullable = false)
    private String firstName;

    @Column(name = "LAST_NAME", length = 40, nullable = false)
    private String lastName;

    @Column(name = "NUMBER", length = 2)
    private int number;

    private int age;

    @Column(name = "BIRTHDAY", length = 10)
    private LocalDate birthDay;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "clubName", column = @Column(name = "CLUB_NAME")),
            @AttributeOverride(name = "city", column = @Column(name = "CLUB_CITY")),
            @AttributeOverride(name = "country", column = @Column(name = "CLUB_COUNTRY"))
    })
    private Club club = new Club();

    @Column(name = "VERSION", nullable = false)
    @Version
    private int version;



    //CONSTRUCTOR

    public Player() {
    }



    //GETTERS AND SETTERS

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Transient
    public int getAge() {
        LocalDate now = LocalDate.now();
        return (int) ChronoUnit.YEARS.between(birthDay, now);
    }

    public void setAge(int age) {
        this.age = age;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }
}
