package nationalSoccerTeams.teams;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Club {

    //VARIABLES
    @Column(name = "CLUB_NAME")
    private String clubName;
    @Column(name = "CLUB_CITY")
    private String city;
    @Column(name = "CLUB_COUNTRY")
    private String country;

    //CONSTRUCTOR

    public Club() {
    }

    //GETTERS AND SETTERS

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
