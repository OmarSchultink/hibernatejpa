package nationalSoccerTeams.teams;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.swing.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PlayerDAO {
    Club club = new Club();
    Player player = new Player();


    String country;
    String position;
    String firstName;
    String lastName;
    String number;
    String birthDay;
    String clubName;
    String city;
    String clubCountry;

    EntityManagerFactory emf = null;
    EntityManager em = null;


    public void savePlayer() {

        try {


            EntityManagerFactory emf = Persistence.createEntityManagerFactory("player");
            EntityManager em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();

            tx.begin();

            country = JOptionPane.showInputDialog("Enter country","Netherlands");
            switch (country) {
                case "Netherlands":
                    player.setCountry(Country.NETHERLANDS);
                    break;
                case "Belgium":
                    player.setCountry(Country.BELGIUM);
                    break;
                default:
                    player.setCountry(Country.NETHERLANDS);
                    break;
            }


            position = JOptionPane.showInputDialog("Enter player position: (G)OALKEEPER, (D)EFENDER, (M)IDFIELDER, (A)TTACKER","A");
            switch (position){
                case "G":
                    player.setPosition(Position.GOALKEEPER);
                    break;
                case "D":
                    player.setPosition(Position.DEFENDER);
                    break;
                case "M":
                    player.setPosition(Position.MIDFIELDER);
                    break;
                case "A":
                    player.setPosition(Position.ATTACKER);
                    break;


            }


            firstName = JOptionPane.showInputDialog("Enter first name","Omar");
            lastName = JOptionPane.showInputDialog("Enter last name","Schultink");
            number = JOptionPane.showInputDialog("Enter number","5");
            int numberInt = Integer.parseInt(number);

            birthDay = JOptionPane.showInputDialog("Enter birthday in dd/MM/yyyy","12/09/1981");
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate localDateBirthDay = LocalDate.parse(birthDay, dateTimeFormatter);

            clubName = JOptionPane.showInputDialog("Enter player's club name","Feyenoord");
            city = JOptionPane.showInputDialog("Enter club's city","Rotterdam");
            clubCountry = JOptionPane.showInputDialog("Enter club's country","Netherlands");

            player.setFirstName(firstName);
            player.setLastName(lastName);
            player.setNumber(numberInt);
            player.setBirthDay(localDateBirthDay);
            player.setAge(player.getAge());
            club.setClubName(clubName);
            club.setCity(city);
            club.setCountry(clubCountry);

            em.persist(player);
            tx.commit();


        } finally {
            if (em != null) {
                em.close();
            }
            if (emf != null) {
                emf.close();

            }
        }

    }
}
