package nationalSoccerTeams.teams;

public enum Position {
    GOALKEEPER, DEFENDER, MIDFIELDER, ATTACKER;
}
