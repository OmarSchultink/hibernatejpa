package visitors;

import messages.Message;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Scanner;

public class SaveVisitor {
    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = null;
        EntityManager entityManager = null;


        try {
            entityManagerFactory = Persistence.createEntityManagerFactory("course");
            entityManager = entityManagerFactory.createEntityManager();
            EntityTransaction entityTransaction = entityManager.getTransaction();


            Scanner keyboard = new Scanner(System.in);
            System.out.println("Enter your name:");
            String name = keyboard.nextLine();
            System.out.println("Enter your age:");
            int age = keyboard.nextInt();
            entityTransaction.begin();
            Visitor visitor = new Visitor();
            visitor.setName(name);
            visitor.setAge(age);
            entityManager.persist(visitor);
            entityTransaction.commit();
            System.out.println("Visitor saved");
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
            if (entityManagerFactory != null) {
                entityManagerFactory.close();
            }
        }


    }
}
