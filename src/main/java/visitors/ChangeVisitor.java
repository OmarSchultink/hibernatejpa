package visitors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Scanner;

public class ChangeVisitor {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Scanner keyboard = new Scanner(System.in);
            System.out.println("Enter primary key:");
            int id = keyboard.nextInt();
            System.out.println("Enter new name:");
            String name = keyboard.next();
        //    em.clear(); HAALT ALLES UIT DE PERSISTENCE CONTEXT
            Visitor visitor = em.find(Visitor.class, id);
         //   em.detach(visitor); ER WORDT NIKS GEWIJZIGD

            System.out.println("text "+visitor.getName());
            System.out.println("mijn naam is omar");

         //   visitor.setName("Omar");
            tx.commit();
            em.close();
        //    visitor.setName("Ramo");

    }finally {
            if (emf != null){
                emf.close();
            }
        }
        }




}
