package pokemonDB.pokemon;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Pokemon {

    private int pokemonId;
    String name;
    int specieId;
    int trainerId;
    int level;
    int xp;

    public Pokemon(){

    }
    @Id
    public int getPokemonId(){
        return pokemonId;
    }

    public void setPokemonId(int pokemonId){
        this.pokemonId =pokemonId;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getSpecieId() {
        return specieId;
    }

    public void setSpecieId(int specieId) {
        this.specieId = specieId;
    }

    public int getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(int trainerId) {
        this.trainerId = trainerId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }
}
