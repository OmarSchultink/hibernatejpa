package credits;

public class CheckBalance {


    public static float result(float balance, float balanceMem){

        float result = balanceMem + balance;

        if (result < 0){
            throw new RuntimeException("Funds not available");
        }



        return result;
    }
}
