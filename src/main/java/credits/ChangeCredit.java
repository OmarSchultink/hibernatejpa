package credits;

import javax.persistence.*;
import javax.swing.*;

public class ChangeCredit implements Runnable {


    @Override
    public void run() {
        changeCredit();
    }


    public static void changeCredit() {

        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {

            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();

            EntityTransaction tx = em.getTransaction();

            tx.begin();
            Credit credit = em.find(Credit.class, 1);
            //  em.lock(credit, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
            //  tx.commit();
            //  em.close();

            String amount = JOptionPane.showInputDialog("Enter amount to add/remove", credit.getBalance());
            float amountInFloat = Float.parseFloat(amount);
            float balanceMem = credit.getBalance();
            System.out.println("balanceMem = " + balanceMem);
            System.out.println("amountInFloat = " + amountInFloat);
            float changedAmount = CheckBalance.result(amountInFloat, balanceMem);
            credit.setBalance(changedAmount);
            System.out.println("changedAmount = " + changedAmount);



          /*  em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();*/

            //  em.merge(credit);
            //  em.persist(credit);
            //   System.out.println("Merged");
            tx.commit();

            System.out.println("balance in Database = " + credit.getBalance());

        } finally {
            if (em != null) {
                em.close();
            }
            if (emf != null) {
                emf.close();
            }
        }
    }


    public static void main(String[] args) {
    }

}
