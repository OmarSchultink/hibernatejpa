package credits;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class CreateCredit {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {

            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();

            tx.begin();
            Credit credit = new Credit();
            credit.setBalance(1000.00F);


            em.persist(credit);
            tx.commit();


        }finally {
            if (em != null){
                em.close();
            }
            if (emf != null){
                emf.close();
            }
        }

    }
}
