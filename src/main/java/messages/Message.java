package messages;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
//@Access(AccessType.FIELD)
public class Message  {


    private long id;
  //  @Column(nullable = true)
    private String text;
 //   @Column(nullable = true)
    private String title;
 //   @Column(nullable = true)
    private String fromOmar;
 //   @Column(nullable = true)
    private String toOmar;



    public Message() {
    }

   public Message(long id, String text) {
        this.id = id;
       // setText(text);
        this.text = text;
    }
/*
    public Message(String from, String text,String title, String to, long id) {
        this.id = id;
        this.text = text;
        this.title = title;
        this.from = from;
        this.to = to;
    }*/


    @Id
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

   // @Access(AccessType.PROPERTY)
    public String getText() {
        return text;
    }

    public void setText(String text) {
       // if (text.length() < 5) {
            this.text = text;
      //  } else throw new RuntimeException("text is too long");
    }
  //  @Access(AccessType.PROPERTY)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFromOmar() {
        return fromOmar;
    }

    public void setFromOmar(String fromOmar) {
        this.fromOmar = fromOmar;
    }

    public String getToOmar() {
        return toOmar;
    }

    public void setToOmar(String toOmar) {
        this.toOmar = toOmar;
    }
}
