package messages;

import org.hibernate.query.Query;

import javax.persistence.*;
import java.util.List;

public class GetMessage {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = null;
        EntityManager entityManager = null;

        try{
            entityManagerFactory = Persistence.createEntityManagerFactory("course");
            entityManager = entityManagerFactory.createEntityManager();
            EntityTransaction entityTransaction = entityManager.getTransaction();

            entityTransaction.begin();
            // GET QUERY IN CACHE
            TypedQuery<Message> query = entityManager.createQuery("select m from Message as m",Message.class);
            List<Message> messages = query.getResultList();

            System.out.println(entityManagerFactory.getCache().contains(Message.class,1L));
            for (Message m: messages){
                System.out.println(m.getText());
            }

            /*
            Message message = entityManager.find(Message.class, 1L);
           // System.out.println(message.getText());

            //ROW AVAILABLE IN CACHE???
            System.out.println(entityManagerFactory.getCache().contains(Message.class,1L));
            entityManagerFactory.getCache().evict(Message.class,1L);
            System.out.println(entityManagerFactory.getCache().contains(Message.class,1L));*/
            entityTransaction.commit();


        }finally {
            if (entityManager != null){
                entityManager.close();
            }
            if (entityManagerFactory !=null){
                entityManagerFactory.close();
            }
        }

    }
}
