package messages;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Scanner;

public class SaveMessage {
    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = null;
        EntityManager entityManager = null;


        try{
        entityManagerFactory = Persistence.createEntityManagerFactory("course");
        entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
            System.out.println("transaction found");
        entityTransaction.begin();
        Message message = new Message(1,"text");
            System.out.println(message.getId());
      /*  message.setText("best");
        message.setId(7);
        message.setTitle("tit");
        message.setFrom("omar");
        message.setTo("daniel");*/
        entityManager.persist(message);
        entityTransaction.commit();
        System.out.println("Message saved");}
        finally {
            if (entityManager != null){
                entityManager.close();
            }
            if (entityManagerFactory != null){
                entityManagerFactory.close();
            }
        }


    }
}
