package players;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

//@Entity  //AANZETTEN OM TERUG TE LATEN WERKEN
@IdClass(PlayerPK.class)
public class Player {
    private String club;
    private String name;
    private int number;

    public Player(String club, String name, int number) {
        this.club = club;
        this.name = name;
        this.number = number;
    }

    public Player() {
    }

    @Id
    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Id
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }


}
