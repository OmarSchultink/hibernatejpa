package players;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Scanner;

public class SavePlayer {
    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = null;
        EntityManager entityManager = null;

        try{
            entityManagerFactory = Persistence.createEntityManagerFactory("course");
            entityManager = entityManagerFactory.createEntityManager();
            EntityTransaction entityTransaction = entityManager.getTransaction();
            entityTransaction.begin();
            Scanner keyboard = new Scanner(System.in);

            System.out.println("Enter number:");
            int number = keyboard.nextInt();

            System.out.println("Enter club:");
            String club = keyboard.next();

            System.out.println("Enter name:");
            String name = keyboard.next();

            Player player = new Player();
            player.setNumber(number);
            player.setClub(club);
            player.setName(name);
            entityManager.persist(player);
            entityTransaction.commit();
        }
        finally {
            if (entityManager != null){
                entityManager.close();
            }
            if (entityManagerFactory != null){
                entityManagerFactory.close();
            }
        }
    }
}
