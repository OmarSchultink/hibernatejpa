package players;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GetPlayer {
    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = null;
        EntityManager entityManager = null;

        try {
            entityManagerFactory = Persistence.createEntityManagerFactory("course");
            entityManager = entityManagerFactory.createEntityManager();
            EntityTransaction entityTransaction = entityManager.getTransaction();

            entityTransaction.begin();
            PlayerPK playerPK = new PlayerPK();
            playerPK.setClub("LA Galaxy");
            playerPK.setNumber(1);
            Player player = entityManager.find(Player.class, playerPK);
            entityTransaction.commit();
            System.out.println(player.getName());
        }

        finally {
            if (entityManager != null){
                entityManager.close();
            }
            if (entityManagerFactory != null){
                entityManagerFactory.close();
            }
        }
    }
}
